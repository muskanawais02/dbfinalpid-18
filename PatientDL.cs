﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WindowsFormsApp1.BL;

namespace WindowsFormsApp1.DL
{
    class PatientDL
    {
        private static PatientDL instance = null;
        private readonly Configuration configuration;

        private PatientDL()
        {
            configuration = Configuration.getInstance();
        }

        public static PatientDL getInstance()
        {
            if (instance == null)
                instance = new PatientDL();

            return instance;
        }

        // Method to fetch urgency levels from the database
     



            // Method to add a new patient to the database
            public bool AddRequest(PatientBL patient)
        {
            // Check if any required fields are empty
            if (string.IsNullOrEmpty(patient.FullName) || string.IsNullOrEmpty(patient.Gender) ||
                patient.DateOfBirth == DateTime.MinValue || string.IsNullOrEmpty(patient.BloodType) ||
                string.IsNullOrEmpty(patient.StreetAddress) || string.IsNullOrEmpty(patient.Province) ||
                string.IsNullOrEmpty(patient.UrgencyLevel) || string.IsNullOrEmpty(patient.MedicalCondition) ||
                string.IsNullOrEmpty(patient.EmailAddress) || string.IsNullOrEmpty(patient.MobileNumber) ||
                string.IsNullOrEmpty(patient.Country) || string.IsNullOrEmpty(patient.HospitalName) ||
                string.IsNullOrEmpty(patient.City) || string.IsNullOrEmpty(patient.PostalCode) ||
                string.IsNullOrEmpty(patient.MedicalConditionRequiringBloodDonation))
            {
                return false; // Return false if any required field is empty
            }

            try
            {
                // Get SqlConnection from Configuration class
                using (SqlConnection connection = configuration.getConnection())
                {
                    // Create a SqlCommand for inserting data into Patients table
                    string query = @"INSERT INTO Patients (FullName, Gender, DateOfBirth, BloodType, StreetAddress, Province, UrgencyLevel, MedicalCondition, 
                                    EmailAddress, MobileNumber, Country, HospitalName, City, PostalCode, MedicalConditionRequiringBloodDonation)
                                    VALUES (@FullName, @Gender, @DateOfBirth, @BloodType, @StreetAddress, @Province, @UrgencyLevel, @MedicalCondition, 
                                    @EmailAddress, @MobileNumber, @Country, @HospitalName, @City, @PostalCode, @MedicalConditionRequiringBloodDonation)";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        // Add parameters
                        command.Parameters.AddWithValue("@FullName", patient.FullName);
                        command.Parameters.AddWithValue("@Gender", patient.Gender);
                        command.Parameters.AddWithValue("@DateOfBirth", patient.DateOfBirth);
                        command.Parameters.AddWithValue("@BloodType", patient.BloodType);
                        command.Parameters.AddWithValue("@StreetAddress", patient.StreetAddress);
                        command.Parameters.AddWithValue("@Province", patient.Province);
                        command.Parameters.AddWithValue("@UrgencyLevel", patient.UrgencyLevel);
                        command.Parameters.AddWithValue("@MedicalCondition", patient.MedicalCondition);
                        command.Parameters.AddWithValue("@EmailAddress", patient.EmailAddress);
                        command.Parameters.AddWithValue("@MobileNumber", patient.MobileNumber);
                        command.Parameters.AddWithValue("@Country", patient.Country);
                        command.Parameters.AddWithValue("@HospitalName", patient.HospitalName);
                        command.Parameters.AddWithValue("@City", patient.City);
                        command.Parameters.AddWithValue("@PostalCode", patient.PostalCode);
                        command.Parameters.AddWithValue("@MedicalConditionRequiringBloodDonation", patient.MedicalConditionRequiringBloodDonation);

                        // Open the connection
                        connection.Open();

                        // Execute the command
                        command.ExecuteNonQuery();
                    }
                }

                return true; // Return true if patient is successfully added
            }
            catch (Exception ex)
            {
                // Handle exceptions
                Console.WriteLine("Error adding patient: " + ex.Message);
                return false;
            }
        }
    }
}
