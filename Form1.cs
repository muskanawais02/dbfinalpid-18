﻿using BloodDonationSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private const int intervalTicks = 100; // Number of ticks before opening the new form
        private int tickCount = 0; // Counter for ticks

        public Form1()
        {
            InitializeComponent();
            SetRoundForm();
        }
        private void SetRoundForm()
        {
            GraphicsPath path = new GraphicsPath();
            // Create a rounded rectangle path
            path.AddArc(0, 0, 50, 50, 180, 90);
            path.AddArc(this.Width - 50, 0, 50, 50, 270, 90);
            path.AddArc(this.Width - 50, this.Height - 50, 50, 50, 0, 90);
            path.AddArc(0, this.Height - 50, 50, 50, 90, 90);
            // Set the form's region to the rounded rectangle path
            this.Region = new Region(path);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            ProgressBar.Left += 100;
            tickCount++;

            // Check if the specified number of ticks has occurred
            if (tickCount >= intervalTicks)
            {
                timer1.Stop(); // Stop the timer
                OpenNewForm(); // Open a new form
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void OpenNewForm()
        {
            this.Hide();
            // Create and show a new instance of Form2
            SignUp form2 = new SignUp();
           // SignUp form2 = new();
            form2.Show();
        }

        private void FYP_Click(object sender, EventArgs e)
        {

        }
    }
}
