﻿using System;
using System.Windows.Forms;
using WindowsFormsApp1.BL;

namespace WindowsFormsApp1.Forms
{
    public partial class EditPersonalInfo : Form
    {
        private readonly UserBL _userBL;
        private readonly int _userId;

        // Properties
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateOfBirth { get; set; }

        // Constructor with parameter
        public EditPersonalInfo(int userId)
        {
            InitializeComponent();
            _userBL = new UserBL();
            _userId = userId;

            // Call a method to load recipient information when the form loads
            LoadRecipientInformation();
        }

        // Default constructor
        public EditPersonalInfo()
        {
            InitializeComponent();
        }

        private void LoadRecipientInformation()
        {
            try
            {
                // Call the method from UserBL to get recipient information
                EditPersonalInfo recipient = _userBL.GetRecipientInfo(_userId);

                // Populate textboxes and comboboxes with recipient information
                if (recipient != null)
                {
                    streetAddressTextBox.Text = recipient.StreetAddress;
                    cityComboBox.SelectedItem = recipient.City;
                    provinceComboBox.SelectedItem = recipient.Province;
                    postalCodeTextBox.Text = recipient.PostalCode;
                    countryComboBox.SelectedItem = recipient.Country;
                    bloodTypeComboBox.SelectedItem = recipient.BloodType;
                    genderComboBox.SelectedItem = recipient.Gender;
                    mobileNumberTextBox.Text = recipient.MobileNumber;
                    emailAddressTextBox.Text = recipient.EmailAddress;
                    dateOfBirthPicker.Value = recipient.DateOfBirth;
                }
                else
                {
                    MessageBox.Show("Recipient information not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error loading recipient information: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

      

        private void Search_Click(object sender, EventArgs e)
        {
            // Retrieve values from textboxes and comboboxes
            string streetAddress = streetAddressTextBox.Text;
            string city = cityComboBox.SelectedItem.ToString();
            string province = provinceComboBox.SelectedItem.ToString();
            string postalCode = postalCodeTextBox.Text;
            string country = countryComboBox.SelectedItem.ToString();
            string bloodType = bloodTypeComboBox.SelectedItem.ToString();
            string gender = genderComboBox.SelectedItem.ToString();
            string mobileNumber = mobileNumberTextBox.Text;
            string emailAddress = emailAddressTextBox.Text;
            string dateOfBirth = dateOfBirthPicker.Value.ToString("yyyy-MM-dd");

            try
            {
                // Call the method from UserBL to update recipient information
                _userBL.UpdateRecipientInfo(_userId, streetAddress, city, province, postalCode, country, bloodType, gender, mobileNumber, emailAddress, dateOfBirth);

                MessageBox.Show("Recipient information updated successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Close the form after successful update
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error updating recipient information: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
