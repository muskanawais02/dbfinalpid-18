﻿using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using WindowsFormsApp1.DL;


namespace BloodDonationSystem
{
    public partial class SignUp : Form
    {
        private readonly UserBL _userBL;
        public SignUp()
        {
            InitializeComponent();

            _userBL = new UserBL(); // Assuming UserBL has a parameterless constructor
        }

       

        private void LoginButton_Click(object sender, EventArgs e)
        {

        }

        private void SignupButton_Click(object sender, EventArgs e)
        {
            // Input validation (assuming you have the necessary controls)
            if (string.IsNullOrEmpty(txtusername.Text))
            {
                MessageBox.Show("Please enter a username.");
                return;
            }

            if (string.IsNullOrEmpty(txtpassword.Text))
            {
                MessageBox.Show("Please enter a password.");
                return;
            }

            if (!Donor.Checked && !Recipient.Checked)
            {
                MessageBox.Show("Please select your user type (Donor or Recipient).");
                return;
            }

            string username = txtusername.Text;
            string password = txtpassword.Text;
            string userType = Donor.Checked ? "Donor" : "Recipient";

            try
            {
                _userBL.RegisterUser(username, password, userType);
              
                

                this.Hide(); // Hide the signup form after successful registration
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error registering user: {ex.Message}");
            }
        }
        private void PassLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (btnHideShow.Text == "show")
            {
                btnHideShow.Text = "hide";
                txtpassword.PasswordChar = '\0';
            }
            else
            {
                btnHideShow.Text = "show";
                txtpassword.PasswordChar = '*';
            }
        }
        

    }

}
