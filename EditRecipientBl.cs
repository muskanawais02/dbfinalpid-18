using BloodDonationSystem;
using Microsoft.VisualBasic.ApplicationServices;
using Org.BouncyCastle.Cms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DL;
using WindowsFormsApp1.Forms;

namespace WindowsFormsApp1.BL
{
   public class UserBL
    {
        private readonly UserDAL _userDAL;

        public UserBL()
        {
            _userDAL = new UserDAL(); // Assuming UserDAL has a parameterless constructor
        }
        public void SaveDonorInfo(int userId, string lastDonationDate, string streetAddress, string city, string province, string postalCode, string country, string willingness, decimal weight, decimal height, string bloodType)
        {
            // Get the ID corresponding to the "Pending" status from the lookup table
            int donorStatusId = GetDonorStatusId("Pending");

            // Save the location and retrieve its ID
            int locationId = _userDAL.SaveLocation(streetAddress, city, province, postalCode, country);

            // Retrieve blood type ID
            int bloodTypeId = GetBloodTypeId(bloodType);

            // Assume currentLocationId is the same as the locationId initially
            int currentLocationId = locationId;

            _userDAL.SaveDonorInfo(userId, lastDonationDate, locationId, currentLocationId, willingness, donorStatusId, weight, height, bloodTypeId);
        }

        public int GetBloodTypeId(string bloodType)
        {
            try
            {
                // Get connection
                var con = Configuration.getInstance().getConnection();

                // Create SQL command to retrieve blood type ID
                SqlCommand cmd = new SqlCommand("SELECT lookup_id FROM Lookup WHERE value = @Value AND category = 'Blood Type'", con);
                cmd.Parameters.AddWithValue("@Value", bloodType);

                // Execute command
                object result = cmd.ExecuteScalar();

                // Check if result is not null
                if (result != null)
                {
                    return Convert.ToInt32(result);
                }
                else
                {
                    // Handle if the blood type is not found
                    throw new Exception("Blood type not found in the lookup table.");
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions
                MessageBox.Show(ex.Message, "Error at GetBloodTypeId", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1; // Or throw an exception
            }
        }

        public int GetDonorStatusId(string status)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT lookup_id FROM Lookup WHERE value = @Value AND category = 'Donor Request Status'", con);
                cmd.Parameters.AddWithValue("@Value", status);
                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    return Convert.ToInt32(result);
                }
                else
                {
                    // Handle if the status is not found
                    return -1; // Or throw an exception
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error at GetDonorStatusId", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1; // Or throw an exception
            }
        }


        public void SavePersonInfo(int userId, string name, string gender, string mobileNumber,
                           string emailAddress, string dateOfBirth)
        {
            // Get the gender ID from the gender value
            int genderId = GetGenderId(gender);

            // Pass genderId to SavePersonInfo method in UserDAL
            _userDAL.SavePersonInfo(userId, name, genderId, mobileNumber, emailAddress, dateOfBirth);
        }

        public void SaveLocation( string streetAddress, string city, string province, string postalCode, string country)
        {
            _userDAL.SaveLocation( streetAddress, city, province, postalCode, country);
        }

        public void SaveMedicalHistory(int userId, string bloodTypeId, string infections, string allergies,
                                       string medications, string currentTreatment, string chronicDiseases, string surgeries)
        {
            _userDAL.SaveMedicalHistory(userId, bloodTypeId, infections, allergies, medications, currentTreatment, chronicDiseases, surgeries);
        }
        public void RegisterUser(string username, string password, string userType)
        {
            // Business logic can be added here, e.g., password complexity checks

            int userId = _userDAL.CreateUser(username, password, userType);
            MessageBox.Show("Registration successful!");

            Form personalInfoForm = GetPersonalInfoForm(userType, userId);
            personalInfoForm.Show();
        }

        public Form GetPersonalInfoForm(string userType, int userId)
        {
            if (userType == "Donor")
            {
                return new DonorPersonalInfo(userId); // Pass the user ID to the form
            }
            else
            {
                return new RecipientPersonalInfo( userId); // Assuming RecipientPersonalInfo form exists
            }
        }
        public int GetGenderId(string gender)
        {
            int genderId = 0;
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT lookup_id FROM Lookup WHERE value = @Value AND category = 'GENDER'", con);
                cmd.Parameters.AddWithValue("@Value", gender);
                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    genderId = Convert.ToInt32(result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error at GetGenderId", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return genderId;
        }


        public void SaveRecipientInfo(int userId, string streetAddress, string city, string province, string postalCode, string country, string bloodType)
        {

            // Get blood type ID from the lookup table
            int bloodTypeId = GetBloodTypeId(bloodType);

            // Save recipient information using UserDAL
            _userDAL.SaveRecipientInfo(userId, streetAddress, city, province, postalCode, country,  bloodTypeId);
        }

        // Method to get urgency level ID
        public int GetUrgencyLevelId(string urgencyLevel)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT lookup_id FROM Lookup WHERE value = @Value AND category = 'Urgency Level'", con);
                cmd.Parameters.AddWithValue("@Value", urgencyLevel);
                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    return Convert.ToInt32(result);
                }
                else
                {
                    // Handle if the urgency level is not found
                    return -1; // Or throw an exception
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error at GetUrgencyLevelId", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1; // Or throw an exception
            }
        }

        // Add a method in UserBL to get recipient information
        public EditPersonalInfo GetRecipientInfo(int userId)
        {
            return _userDAL.GetRecipientInfo(userId);
        }

        // Add a method in UserBL to update recipient information
        public void UpdateRecipientInfo(int userId, string streetAddress, string city, string province, string postalCode, string country, string bloodType, string gender, string mobileNumber, string emailAddress, string dateOfBirth)
        {
            _userDAL.UpdateRecipientInfo(userId, streetAddress, city, province, postalCode, country, bloodType, gender, mobileNumber, emailAddress, dateOfBirth);
        }




    }
}
