using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Forms
{
    public partial class ViewDonor : Form
    {
        private string connectionString;
        public ViewDonor()
        {
            InitializeComponent();
            SqlConnection connection = Configuration.getInstance().getConnection();
            connectionString = connection.ConnectionString;
            LoadData();

        }



        private void LoadData()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string query = @"
            SELECT 
                p.name AS DonorName,
                l.city + ', ' + l.province + ', ' + l.country AS Location,
                DATEDIFF(YEAR, p.date_of_birth, GETDATE()) AS Age,
                (SELECT value FROM Lookup WHERE lookup_id = d.blood_type) AS BloodType,
                (SELECT COUNT(*) FROM BloodRequestHistory WHERE recipient_id = d.donor_id) AS DonationCount,
                mh.currentTreatment AS CurrentTreatment
            FROM 
                Donors d
            JOIN 
                Person p ON d.donor_id = p.person_id
            JOIN 
                Locations l ON d.location_id = l.location_id
            LEFT JOIN 
                MedicalHistory mh ON p.person_id = mh.medical_history_id;
        ";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        DataTable dataTable = new DataTable();
                        adapter.Fill(dataTable);

                        // Bind the DataTable to the DataGridView
                        guna2DataGridView1.DataSource = dataTable;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading data: " + ex.Message);
            }
        }



        private void Search_Click(object sender, EventArgs e)
        {
            string searchName = SearchText.Text.Trim();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string query = @"
                SELECT 
                    p.name AS DonorName,
                    l.city + ', ' + l.province + ', ' + l.country AS Location,
                    DATEDIFF(YEAR, p.date_of_birth, GETDATE()) AS Age,
                    (SELECT value FROM Lookup WHERE lookup_id = d.blood_type) AS BloodType,
                    (SELECT COUNT(*) FROM BloodRequestHistory WHERE recipient_id = d.donor_id) AS DonationCount,
                    mh.currentTreatment AS CurrentTreatment
                FROM 
                    Donors d
                JOIN 
                    Person p ON d.donor_id = p.person_id
                JOIN 
                    Locations l ON d.location_id = l.location_id
                LEFT JOIN 
                    MedicalHistory mh ON p.person_id = mh.medical_history_id
                WHERE 
                    p.name LIKE @searchName;
            ";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        // Add parameter for searchName
                        command.Parameters.AddWithValue("@searchName", "%" + searchName + "%");

                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        DataTable dataTable = new DataTable();
                        adapter.Fill(dataTable);

                        // Check if any rows were returned
                        if (dataTable.Rows.Count > 0)
                        {
                            // Donor(s) found, display data in DataGridView
                            guna2DataGridView1.DataSource = dataTable;
                        }
                        else
                        {
                            // No donor found, display message
                            MessageBox.Show("No such data found.", "Search Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error searching data: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
