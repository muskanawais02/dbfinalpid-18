﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Validation
    {

        public static bool ValidatePassword(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return false; // Empty password is invalid
            }

            // Minimum length requirement
            if (password.Length < 8)
            {
                return false; // Password too short
            }

            // Enforce complexity with character categories
            int minLowerCase = 1;
            int minUpperCase = 1;
            int minDigit = 1;
            int minSpecialChar = 1;

            bool hasLowerCase = password.Any(c => char.IsLower(c));
            bool hasUpperCase = password.Any(c => char.IsUpper(c));
            bool hasDigit = password.Any(c => char.IsDigit(c));
            bool hasSpecialChar = password.Any(c => char.IsSymbol(c) || char.IsPunctuation(c));

            int complexityMet = 0;
            complexityMet += hasLowerCase ? 1 : 0;
            complexityMet += hasUpperCase ? 1 : 0;
            complexityMet += hasDigit ? 1 : 0;
            complexityMet += hasSpecialChar ? 1 : 0;

            return complexityMet >= (minLowerCase + minUpperCase + minDigit + minSpecialChar);
        }

        public static bool ValidateUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return false; // Empty username is invalid
            }

            
            if (username.Length < 6)
            {
                return false; // Username too short
            }

            
            string pattern = @"^[a-zA-Z0-9_]+$";
            return Regex.IsMatch(username, pattern);
        }

        public static bool FirstNameValidations(string name)
        {
            if (name == "" || name == " ")
            {
                MessageBox.Show("First Name cannot be empty", "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            bool nameValid = true;
            string Alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
            foreach (char n in name)
            {
                if (!Alphabets.Contains(n.ToString()))
                {
                    MessageBox.Show("First Name can only be Alphabets", "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
                    nameValid = false;
                }
            }
            return nameValid;
        }


        public static bool ContactValidations(string contact)
        {
            if (contact == "")
            {
                return true;
            }
            string numb = contact.Substring(4, contact.Length - 4);
            string numbers = "0123456789";
            if (contact[0] == '+' && contact[1] == '9' && contact[2] == '2' && contact[3] == '-' && contact[4] == '3' && contact.Length == 14)
            {
                foreach (char n in numb)
                {
                    if (!numbers.Contains(n.ToString()))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                MessageBox.Show("Contact Format should be +92-3xxxxxxxxx", "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }



        public static bool EmailValidations(string email)
        {
            if (email.Contains("@") && email.Contains("."))
            {
                return true;
            }
            MessageBox.Show("Provided email is not valid", "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        public static bool DoBValidations(string dob, int minYear, int maxYear)
        {
            if (dob == "")
            {
                MessageBox.Show(" You Haven't Selected Date of Birth Please select it first ", "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            bool isValid = true;

            if (DateTime.Parse(dob).Year > maxYear)
            {
                MessageBox.Show("Date Of Birth's Year cannot be more than " + maxYear, "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (DateTime.Parse(dob).Year < minYear)
            {
                MessageBox.Show("Date Of Birth's Year cannot be less than " + minYear, "Error", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return isValid;
        }

        public static bool AddressValidations(string streetAddress, string city, string province, string postalCode, string country)
        {
            if (string.IsNullOrWhiteSpace(streetAddress) ||
                string.IsNullOrWhiteSpace(city) ||
                string.IsNullOrWhiteSpace(province) ||
                string.IsNullOrWhiteSpace(postalCode) ||
                string.IsNullOrWhiteSpace(country))
            {
                MessageBox.Show("Address fields cannot be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

         
            if (!IsValidPostalCode(postalCode))
            {
                MessageBox.Show("Postal code must be in a valid format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        
            return true;
        }

        public static bool BloodTypeValidations(string bloodTypeId)
        {
           
            if (string.IsNullOrWhiteSpace(bloodTypeId))
            {
                MessageBox.Show("Blood type must be selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

         
            return true;
        }

        private static bool IsValidPostalCode(string postalCode)
        {
            // Validate postal code format (e.g., for US)
            // You can adjust this regex pattern according to the format required for your application
            string pattern = @"^\d{5}(?:[-\s]\d{4})?$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(postalCode);
        }

        public static bool ValidateInfections(string infections)

        {
            return !string.IsNullOrEmpty(infections); // Ensure it's not empty or null
        }

        public static bool ValidateAllergies(string allergies)

        {
            return !string.IsNullOrEmpty(allergies);
        }

        public static bool ValidateMedications(string medications)

        {
           return !string.IsNullOrEmpty(medications);

        }

        public static bool ValidateCurrentTreatment(string currentTreatment)

        {
            return !string.IsNullOrEmpty(currentTreatment);
        }

        public static bool ValidateChronicDiseases(string chronicDiseases)

        {
            return !string.IsNullOrEmpty(chronicDiseases);
        }

        public static bool ValidateSurgeries(string surgeries)

        {
            return !string.IsNullOrEmpty(surgeries);
        }

        public static bool ValidateGender(string gender)

        {

            if (string.IsNullOrEmpty(gender))
            {
                return false; // Empty gender is invalid
            }
            string[] validGenders = { "Male", "Female" }; // Adjust based on your options
            return validGenders.Contains(gender);
        }



        public static bool ValidateLastDonationDate(string lastDonationDate)

        {

            if (string.IsNullOrEmpty(lastDonationDate))

            {

                return false; // Empty date is invalid

            }



            try

            {

                DateTime date = DateTime.Parse(lastDonationDate);

                return date <= DateTime.Today; // Donation date cannot be in the future

            }

            catch (FormatException)

            {

                return false; // Invalid date format

            }

        }

        public static bool ValidateWillingness(string willingness)

        {

            if (string.IsNullOrEmpty(willingness))

            {

                return false; // Empty willingness selection is invalid

            }
            string[] validOptions = { "Yes", "No" }; // Adjust based on your options

            return validOptions.Contains(willingness);

        }

        public static bool ValidateWeight(decimal weight)

        {

            if (weight <= 0)

            {

                return false; // Weight cannot be zero or negative

            }

            return true; // Basic validation, weight must be positive

        }

        public static bool ValidateHeight(decimal height)

        {

            if (height <= 0)

            {

                return false; // Height cannot be zero or negative

            }
            return true; // Basic validation, height must be positive

        }






    }
}


