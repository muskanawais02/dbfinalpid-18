using BloodDonationSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1
{
    public partial class DonorMenu : Form
    {
        //int donorID = SessionManager.DonorId;
        private readonly string _connectionString;
        private readonly UserDAL _userDAL;

        public DonorMenu()
        {
            InitializeComponent();
            _userDAL = new UserDAL();
            // Call the function to set hover effects for all buttons
            SetHoverEffectsForButtons(this);
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
        
            // Store original colors (assuming all buttons have the same defaults)
            originalBackColor = DashBoard.BackColor;
            originalForeColor = DashBoard.ForeColor;
        }

        private Form activeForm;

        private Color originalBackColor; // Store original button back color
        private Color originalForeColor; // Store original button fore color

        private bool IsDonorApproved()
        {
            int donorId = SessionManager.DonorId; // Implement this method to get the logged-in donor's ID

            // Query the database to check the donor's status
            string query = "SELECT donor_status FROM Donors WHERE donor_id = @DonorId";

            // Execute the query and check if the status is 'Approved'
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@DonorId", donorId);

                int donorStatus = (int)command.ExecuteScalar();
                string donorStatusString = _userDAL.GetLookupValue("Donor Request Status", donorStatus);

                return donorStatusString == "Approved";
            }
        }


        public void OpenChildForm(Form childForm, object sender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }

            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktopPane.Controls.Add(childForm);
            this.panelDesktopPane.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            label1.Text = childForm.Text;

            // ... rest of your OpenChildForm code
        }
        private void SetHoverEffectsForButtons(Control container)
        {
            // Loop through all controls in the container
            foreach (Control control in container.Controls)
            {
                // If the control is a container (like Panel or GroupBox), recursively call SetHoverEffectsForButtons
                if (control.HasChildren)
                {
                    SetHoverEffectsForButtons(control);
                }

                // Check if the control is a button
                if (control is Button button)
                {
                    // Add event handlers for the button
                    button.MouseEnter += Button_MouseEnter;
                    button.MouseLeave += Button_MouseLeave;
                }
            }
        }

        private void Button_MouseEnter(object sender, EventArgs e)
        {
            Button button = (Button)sender; // Cast sender to Button

            // Change back and fore color on hover (adjust colors as desired)
            button.BackColor = Color.DimGray;
            button.ForeColor = Color.White;
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender; // Cast sender to Button

            // Reset back and fore color to original on leave
            button.BackColor = originalBackColor;
            button.ForeColor = originalForeColor;
        }

        private void DashBoard_Click(object sender, EventArgs e)
        {
            if (IsDonorApproved())
            {
                OpenChildForm(new Forms.DonorsDashBoard(), sender);
            }
            else
            {
                MessageBox.Show("Your donor request is not yet approved. You cannot access this feature.");
            }
        }

        private void Profile_Click(object sender, EventArgs e)
        {

            if (IsDonorApproved())
            {
                OpenChildForm(new Forms.DonorProfile(), sender);
            }
            else
            {
                MessageBox.Show("Your donor request is not yet approved. You cannot access this feature.");
            }
           

        }

        private void Donors_Click(object sender, EventArgs e)
        {

            if (IsDonorApproved())
            {
                OpenChildForm(new Forms.EditDonorInfo(), sender);
            }
            else
            {
                MessageBox.Show("Your donor request is not yet approved. You cannot access this feature.");
            }
            
        }

        private void BloodRequests_Click(object sender, EventArgs e)
        {

            if (IsDonorApproved())
            {
                OpenChildForm(new Forms.Donorhistory(), sender);
            }
            else
            {
                MessageBox.Show("Your donor request is not yet approved. You cannot access this feature.");
            }
           
        }

        private void Donations_Click(object sender, EventArgs e)
        {
         //   OpenChildForm(new Forms.RequestDetails(), sender);

        }

        private void Recipient_Click(object sender, EventArgs e)
        {

            if (IsDonorApproved())
            {
                OpenChildForm(new Forms.RecipientRequest(), sender);
            }
            else
            {
                MessageBox.Show("Your donor request is not yet approved. You cannot access this feature.");
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (IsDonorApproved())
            {
                Location locationForm = new Location();
                locationForm.Show();
            }
            else
            {
                MessageBox.Show("Your donor request is not yet approved. You cannot access this feature.");
            }
           

        }

        private void Reports_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }




}
