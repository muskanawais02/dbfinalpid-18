﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1.Forms
{
    public partial class ViewFeedBacks : Form
    {

        private readonly FeedBackDL _feedbackDL;

        private readonly string _connectionString;

      
        public ViewFeedBacks()
        {
            InitializeComponent();
            _feedbackDL = new FeedBackDL();
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
            PopulateFeedbackData();
        }
        private void PopulateFeedbackData()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string query = @"SELECT p.name AS UserName, f.feedback_text AS FeedbackText, f.feedback_date AS SubmissionDate
                             FROM Feedback f
                             INNER JOIN Recipients r ON f.feedback_id = r.recipient_id
                             INNER JOIN Person p ON r.recipient_id = p.person_id";
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);

                    guna2DataGridView1.DataSource = dataTable;

                    // Set column headers
                    guna2DataGridView1.Columns[0].HeaderText = "User Name";
                    guna2DataGridView1.Columns[1].HeaderText = "Feedback Text";
                    guna2DataGridView1.Columns[2].HeaderText = "Submission Date";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
