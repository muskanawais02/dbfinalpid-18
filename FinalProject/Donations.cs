using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Forms
{
    public partial class Donations : Form
    {
        private readonly string _connectionString;

        public Donations()
        {
            InitializeComponent();
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
            PopulateDonationsData();
        }
        private void PopulateDonationsData()
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string query = @"
                SELECT
                    p1.name AS DonorName,
                    (SELECT value FROM Lookup WHERE lookup_id = don.blood_type) AS DonorBloodType,
                    p2.name AS RecipientName,
                    pat.name AS PatientName,
                    (SELECT value FROM Lookup WHERE lookup_id = pat.blood_type) AS PatientBloodType,
                    d.donation_date,
                    l.street_address + ', ' + l.city + ', ' + l.province + ', ' + l.country + ', ' + l.postal_code AS Address
                FROM
                    Donations d
                JOIN
                    Donors don ON d.donor_id = don.donor_id
                JOIN
                    Person p1 ON don.donor_id = p1.person_id
                JOIN
                    BloodRequestHistory h ON d.history_id = h.history_id
                JOIN
                    Person p2 ON h.recipient_id = p2.person_id
                JOIN
                    Patients pat ON h.patient_id = pat.patient_id
                JOIN
                    Locations l ON pat.current_location_id = l.location_id
            ";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Clear existing columns
                            guna2DataGridView1.Columns.Clear();
                            guna2DataGridView1.DataSource = dataTable;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
