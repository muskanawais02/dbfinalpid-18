using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;

namespace WindowsFormsApp1.Forms
{
    public partial class Donorhistory : Form
    {
        private string _connectionString;

        public Donorhistory()
        {
            InitializeComponent();
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
            LoadDonorHistory();
            UpdateTimeLeftText();
        }

        private void LoadDonorHistory()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Retrieve the donor's ID from the SessionManager or any other source
                int donorId = SessionManager.DonorId;

                // Fetch the donor's donation history
                string getDonorHistoryQuery = @"
            SELECT
                p.name AS DonorName,
                p_patient.name AS PatientName,
                loc.street_address AS Address,
                p_patient.hospital_Name AS HospitalName,
                d.donation_date
            FROM Donations d
            INNER JOIN Donors do ON d.donor_id = do.donor_id
            INNER JOIN Person p ON do.donor_id = p.person_id
            INNER JOIN BloodRequestHistory br ON d.history_id = br.history_id
            INNER JOIN Recipients r ON br.recipient_id = r.recipient_id
            INNER JOIN Patients p_patient ON br.patient_id = p_patient.patient_id
            INNER JOIN Locations loc ON p_patient.current_location_id = loc.location_id
            WHERE d.donor_id = @donorId
            ORDER BY d.donation_date DESC";

                SqlCommand getDonorHistoryCommand = new SqlCommand(getDonorHistoryQuery, connection);
                getDonorHistoryCommand.Parameters.AddWithValue("@donorId", donorId);
                SqlDataReader reader = getDonorHistoryCommand.ExecuteReader();

                // Clear the existing data in the DataGridView
                guna2DataGridView1.Rows.Clear();

                // Add column headers
                guna2DataGridView1.Columns.Clear();
                guna2DataGridView1.Columns.Add("DonorNameColumn", "Donor Name");
                guna2DataGridView1.Columns.Add("PatientNameColumn", "Patient Name");
                guna2DataGridView1.Columns.Add("AddressColumn", "Address");
                guna2DataGridView1.Columns.Add("HospitalNameColumn", "Hospital Name");
                guna2DataGridView1.Columns.Add("DonationDateColumn", "Donation Date");

                // Add the data to the DataGridView
                while (reader.Read())
                {
                    guna2DataGridView1.Rows.Add(
                        reader["DonorName"].ToString(),
                        reader["PatientName"].ToString(),
                        reader["Address"].ToString(),
                        reader["HospitalName"].ToString(),
                        ((DateTime)reader["donation_date"]).ToString("yyyy-MM-dd HH:mm:ss")
                    );
                }

                reader.Close();
            }
        }
        private void UpdateTimeLeftText()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Retrieve the donor's ID from the SessionManager or any other source
                int donorId = SessionManager.DonorId;

                // Fetch the donor's latest donation date
                string getLatestDonationDateQuery = @"
            SELECT TOP 1 donation_date
            FROM Donations
            WHERE donor_id = @donorId
            ORDER BY donation_date DESC";

                SqlCommand getLatestDonationDateCommand = new SqlCommand(getLatestDonationDateQuery, connection);
                getLatestDonationDateCommand.Parameters.AddWithValue("@donorId", donorId);
                object latestDonationDateResult = getLatestDonationDateCommand.ExecuteScalar();

                if (latestDonationDateResult != null && latestDonationDateResult != DBNull.Value)
                {
                    DateTime latestDonationDate = (DateTime)latestDonationDateResult;

                    // Calculate the time difference between the latest donation date and the current date
                    TimeSpan timeSinceLastDonation = DateTime.Now - latestDonationDate;

                    // Check if at least three months have passed since the last donation
                    if (timeSinceLastDonation.TotalDays >= 90)
                    {
                        TimeLeftText.Text = "You can donate now!";
                    }
                    else
                    {
                        // Calculate the remaining time until the donor can donate again
                        TimeSpan timeLeftUntilNextDonation = TimeSpan.FromDays(90) - timeSinceLastDonation;

                        TimeLeftText.Text = $" {timeLeftUntilNextDonation.Days} days";
                    }
                }
                else
                {
                    // Donor has not made any donations yet
                    TimeLeftText.Text = "You haven't made any donations yet.";
                }
            }
        }
        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Search_Click(object sender, EventArgs e)
        {

        }
    }
}
