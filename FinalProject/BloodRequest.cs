﻿using Microsoft.VisualBasic.ApplicationServices;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using WindowsFormsApp1.DL;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
namespace WindowsFormsApp1.Forms
{
    public partial class BloodRequest : Form
    {
        private readonly UserBL _userBL;
        private readonly UserDAL _userDL;
        private int _userId;


        public BloodRequest() // Remove the userId parameter
        {
            InitializeComponent();
            _userBL = new UserBL();
            _userDL = new UserDAL();

        }


        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string streetAddress = streetAddressTextBox.Text;
                string city = cityComboBox.SelectedItem.ToString();
                string province = provinceCombox.SelectedItem.ToString();
                string postalCode = postalCodeTextBox.Text;
                string country = countryComboBox.SelectedItem.ToString();

                string bloodTypeId = bloodTypeComboBox.SelectedItem.ToString();

                string name = nameTextBox.Text;
                string gender = genderComboBox.SelectedItem.ToString();
                string mobileNumber = mobileNumberTextBox.Text;
                string emailAddress = emailAddressTextBox.Text;
                string dateOfBirth = dateOfBirthPicker.Value.ToString("yyyy-MM-dd"); // Ensure you have a valid date format
                string hospitalName = HospitalNametextBox.Text;
                string treatment = TreatmenttextBox.Text;
                string urgencyLevel = UrgencyLevelcomboBox.SelectedItem.ToString();
                string attendingPhysician = PhysicianTextBox.Text;
                string status = "Waiting";


                // Save patient info and get patient ID
                int patientId = _userBL.SavePatientInfo(name, gender, mobileNumber, emailAddress, dateOfBirth, bloodTypeId, hospitalName, treatment, attendingPhysician, urgencyLevel, streetAddress, city, province, postalCode, country);

                // Add blood request
                int recipientId = SessionManager.RecipientId;

                DateTime requestDate = DateTime.Now; // Get the current date and time

                if (recipientId > 0)
                {
                    _userBL.AddBloodRequestHistory(recipientId, patientId, requestDate, status, bloodTypeId);
                }
                else
                {
                    MessageBox.Show("Invalid user ID. Unable to save blood request history.");
                }

                // Update recipient table with patient ID
                //  _userDL.UpdateRecipientWithPatientId(recipientId, BloodRequest_id, patientId);

                MessageBox.Show("Request saved successfully!");

                this.Close();

                // Open the sign-in form
                SignIn signInForm = new SignIn();
                signInForm.Show();
            }
            catch (Exception ex)
            {
                // Handle any exceptions
                MessageBox.Show($"An error occurred: {ex.Message}", "Error in saving the blood request", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



    }
}
