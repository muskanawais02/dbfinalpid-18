﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RecipientMenu : Form
    {
        public RecipientMenu()
        {
            InitializeComponent();

            // Call the function to set hover effects for all buttons
            SetHoverEffectsForButtons(this);

            // Store original colors (assuming all buttons have the same defaults)
            originalBackColor = AddRequest.BackColor;
            originalForeColor = AddRequest.ForeColor;
        }
        private Button currentButton;
        private Random random;
        private int tempIndex;
        private Form activeForm;

        private Color originalBackColor; // Store original button back color
        private Color originalForeColor; // Store original button fore color

      

        public void OpenChildForm(Form childForm, object sender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }

            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktopPane.Controls.Add(childForm);
            this.panelDesktopPane.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            label1.Text = childForm.Text;

            // ... rest of your OpenChildForm code
        }
        private void SetHoverEffectsForButtons(Control container)
        {
            // Loop through all controls in the container
            foreach (Control control in container.Controls)
            {
                // If the control is a container (like Panel or GroupBox), recursively call SetHoverEffectsForButtons
                if (control.HasChildren)
                {
                    SetHoverEffectsForButtons(control);
                }

                // Check if the control is a button
                if (control is Button button)
                {
                    // Add event handlers for the button
                    button.MouseEnter += Button_MouseEnter;
                    button.MouseLeave += Button_MouseLeave;
                }
            }
        }

        private void Button_MouseEnter(object sender, EventArgs e)
        {
            Button button = (Button)sender; // Cast sender to Button

            // Change back and fore color on hover (adjust colors as desired)
            button.BackColor = Color.DimGray;
            button.ForeColor = Color.White;
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender; // Cast sender to Button

            // Reset back and fore color to original on leave
            button.BackColor = originalBackColor;
            button.ForeColor = originalForeColor;
        }

        private void Donations_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.AddFeedBack(), sender);
        }

        private void EditRequest_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.EditRequest(), sender);
        }

        private void Locations_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.TrackLocations(), sender);
        }

        private void AddRequest_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.BloodRequest(), sender);
        }

        private void EditProfile_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.EditPersonalInfo(), sender);
        }
    }
}
