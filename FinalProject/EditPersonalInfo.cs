﻿using Org.BouncyCastle.Cms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1.Forms
{
    public partial class EditPersonalInfo : Form
    {
        private readonly UserDAL _userDAL;
        private readonly UserBL _userBL;
        private readonly string _connectionString;

        public EditPersonalInfo()
        {
            InitializeComponent();
            _userDAL = new UserDAL();
            _userBL = new UserBL(); 
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
            PopulateFormData();
        }

        private void PopulateFormData()
        {
            try
            {
                int recipientId = SessionManager.RecipientId;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Fetch recipient ID
                    string selectLocationIdQuery = "SELECT location_id FROM Recipients WHERE recipient_id = @recipientId";
                    SqlCommand locationIdCommand = new SqlCommand(selectLocationIdQuery, connection);
                    locationIdCommand.Parameters.AddWithValue("@recipientId", recipientId);
                    int locationId = Convert.ToInt32(locationIdCommand.ExecuteScalar());

                    // Fetch recipient's personal information
                    string selectPersonQuery = "SELECT p.name, p.gender, p.mobile_number, p.email_address, p.date_of_birth, r.blood_type " +
                                               "FROM Person p " +
                                               "JOIN Recipients r ON p.person_id = r.recipient_id " +
                                               "WHERE r.recipient_id = @recipientId";

                    SqlCommand command = new SqlCommand(selectPersonQuery, connection);
                    command.Parameters.AddWithValue("@recipientId", recipientId);

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        // Populate text boxes and combo boxes with fetched data
                        nameTextBox.Text = reader["name"].ToString();
                        genderComboBox.SelectedItem = _userDAL.GetLookupValue("Gender", Convert.ToInt32(reader["gender"]));
                        mobileNumberTextBox.Text = reader["mobile_number"].ToString();
                        emailAddressTextBox.Text = reader["email_address"].ToString();
                        dateOfBirthPicker.Value = Convert.ToDateTime(reader["date_of_birth"]);
                        bloodTypeComboBox.SelectedItem = _userDAL.GetLookupValue("Blood Type", Convert.ToInt32(reader["blood_type"]));

                        // Close previous data reader before executing new query
                        reader.Close();

                        // Fetch location data based on location ID
                        string selectLocationQuery = "SELECT city, postal_code, street_address, province, country " +
                                                      "FROM Locations " +
                                                      "WHERE location_id = @locationId";

                        SqlCommand locationCommand = new SqlCommand(selectLocationQuery, connection);
                        locationCommand.Parameters.AddWithValue("@locationId", locationId);

                        SqlDataReader locationReader = locationCommand.ExecuteReader();

                        if (locationReader.Read())
                        {
                            cityComboBox.SelectedItem = locationReader["city"].ToString();
                            provinceComboBox.SelectedItem = locationReader["province"].ToString();
                            postalCodeTextBox.Text = locationReader["postal_code"].ToString();
                            streetAddressTextBox.Text = locationReader["street_address"].ToString(); // Remove extra space after "street_address"
                            countryComboBox.SelectedItem = locationReader["country"].ToString();
                        }

                        locationReader.Close();
                    }

                    reader.Close(); // Ensure reader is closed even if no data is fetched
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error fetching recipient information: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate user input (optional)
                // You can add checks for empty fields, invalid formats, etc.

                int recipientId = SessionManager.RecipientId;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Update Person table
                    string updatePersonQuery = "UPDATE Person SET " +
                                          "name = @name, " +
                                          "gender = @gender, " +
                                          "mobile_number = @mobileNumber, " +
                                          "email_address = @emailAddress, " +
                                          "date_of_birth = @dateOfBirth " +
                                          "WHERE person_id = (SELECT person_id FROM Recipients WHERE recipient_id = @recipientId)";

                    SqlCommand updatePersonCommand = new SqlCommand(updatePersonQuery, connection);
                    updatePersonCommand.Parameters.AddWithValue("@name", nameTextBox.Text);

                    // Get and store gender ID
                    int genderId = _userBL.GetGenderId(genderComboBox.SelectedItem.ToString());
                    updatePersonCommand.Parameters.AddWithValue("@gender", genderId);

                    // Get and store blood type ID (assuming similar GetBloodTypeId function)
                    int bloodTypeId = _userBL.GetBloodTypeId(bloodTypeComboBox.SelectedItem.ToString());
                    updatePersonCommand.Parameters.AddWithValue("@blood_type", bloodTypeId); // Add a new parameter for blood type

                    updatePersonCommand.Parameters.AddWithValue("@mobileNumber", mobileNumberTextBox.Text);
                    updatePersonCommand.Parameters.AddWithValue("@emailAddress", emailAddressTextBox.Text);
                    updatePersonCommand.Parameters.AddWithValue("@dateOfBirth", dateOfBirthPicker.Value);
                    updatePersonCommand.Parameters.AddWithValue("@recipientId", recipientId);


                    updatePersonCommand.ExecuteNonQuery();

                    // Update Location table (if applicable)
                    // Update Location table (if applicable)
                    if (cityComboBox.SelectedItem != null && provinceComboBox.SelectedItem != null && countryComboBox.SelectedItem != null)
                    {
                       //  int recipientId = SessionManager.RecipientId;

                        string updateLocationQuery = "UPDATE Locations SET " +
                                                     "city = @city, " +
                                                     "postal_code = @postalCode, " +
                                                     "street_address = @streetAddress, " +
                                                     "province = @province, " +
                                                     "country = @country " +
                                                     "WHERE location_id = (SELECT location_id FROM Recipients WHERE recipient_id = @recipientId)";

                        SqlCommand updateLocationCommand = new SqlCommand(updateLocationQuery, connection);
                        updateLocationCommand.Parameters.AddWithValue("@city", cityComboBox.SelectedItem.ToString());
                        updateLocationCommand.Parameters.AddWithValue("@postalCode", postalCodeTextBox.Text);
                        updateLocationCommand.Parameters.AddWithValue("@streetAddress", streetAddressTextBox.Text);
                        updateLocationCommand.Parameters.AddWithValue("@province", provinceComboBox.SelectedItem.ToString());
                        updateLocationCommand.Parameters.AddWithValue("@country", countryComboBox.SelectedItem.ToString());
                        updateLocationCommand.Parameters.AddWithValue("@recipientId", recipientId);

                        updateLocationCommand.ExecuteNonQuery();
                    }


                    MessageBox.Show("Recipient information updated successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // Clear the form or close the window (optional)
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error updating recipient information: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
