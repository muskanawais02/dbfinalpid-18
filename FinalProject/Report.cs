﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf.qrcode;
using Org.BouncyCastle.Cms;
using static System.ComponentModel.Design.ObjectSelectorEditor;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using System.Reflection;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1.Forms
{
    public partial class Report : Form
    {
        private iTextSharp.text.Font boldFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16, iTextSharp.text.Font.BOLD);
        private iTextSharp.text.Font textFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12);

        public Report()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void GeneratePDFReport(string fileName, string query, Configuration config)
        {
            try
            {
                // Create new PDF document
                Document document = new Document();
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));
                document.Open();

                // Execute query to fetch data from the database
                SqlConnection con = config.getConnection();
                bool connectionOpenedHere = false;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    connectionOpenedHere = true;
                }
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();

                // Create a table to hold the data
                PdfPTable table = new PdfPTable(reader.FieldCount);
                table.WidthPercentage = 100;

                // Add table headers
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BackgroundColor = new BaseColor(128, 128, 128);
                    table.AddCell(cell);
                }

                // Add data rows
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader[i].ToString()));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                    }
                }

                // Add table to the document
                document.Add(table);

                // Close connections and document
                reader.Close();
                if (connectionOpenedHere)
                    con.Close();

                // Close the document
                document.Close();
                writer.Close();

                // Open the generated PDF file
                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error generating report: " + ex.Message);
            }
        }

        private void btnRecipientFeedback_Click(object sender, EventArgs e)
        {

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "PDF Files|*.pdf";
                saveFileDialog.Title = "Save PDF Report";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    // Generate the report and save it at the chosen location
                    GenerateRecipientFeedBAck(saveFileDialog.FileName);
                }
            }
        }
        private void GenerateRecipientFeedBAck(string filepath)
        {
            string query = @"SELECT p.name AS UserName, f.feedback_text AS FeedbackText, f.feedback_date AS SubmissionDate
                             FROM Feedback f
                             INNER JOIN Recipients r ON f.feedback_id = r.recipient_id
                             INNER JOIN Person p ON r.recipient_id = p.person_id";
            Configuration config = Configuration.getInstance();
            GeneratePDFReport("CLO_Wise_Result_Report.pdf", query, config);
        }

    }
}
