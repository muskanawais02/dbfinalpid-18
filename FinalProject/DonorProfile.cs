using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;

namespace WindowsFormsApp1.Forms
{
    public partial class DonorProfile : Form
    {
        int donorID = SessionManager.DonorId;
        private readonly string _connectionString;

       
        public DonorProfile()
        {
            InitializeComponent();
            UserName.ReadOnly = true;
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
            DisplayUserInfo();

        }
        private void DisplayUserInfo()
        {
           
            string query = "SELECT username, password FROM Users WHERE user_id = @userId";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@userId", donorID);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        // Display username in read-only text box
                        UserName.Text = reader["username"].ToString();

                        // Display password in editable text box
                        passordtxt.Text = reader["password"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("User not found.");
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error retrieving user information: " + ex.Message);
                }
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            // Update user's password in the database
            string newPassword = passordtxt.Text;

          
            string query = "UPDATE Users SET password = @password WHERE user_id = @userId";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@password", newPassword);
                command.Parameters.AddWithValue("@userId", donorID);

                try
                {
                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Password updated successfully.");
                    }
                    else
                    {
                        MessageBox.Show("Password update failed.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error updating password: " + ex.Message);
                }
            }
        }
    }
}
