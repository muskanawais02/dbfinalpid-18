using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace WindowsFormsApp1.Forms
{
    public partial class DonorsDashBoard : Form
    {
        int donorID = SessionManager.DonorId;
        private readonly string _connectionString;

        public DonorsDashBoard()
        {
            InitializeComponent();
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
            DisplayRequestCounts();
        }

        private void DisplayRequestCounts()
        {
            int totalRequests = GetTotalRequests();
            int approvedRequests = GetApprovedRequests();

            TotalRequests.Text = totalRequests.ToString();
            ApprovedRequest.Text = approvedRequests.ToString();
        }

        private int GetTotalRequests()
        {
            int totalRequests = 0;
            int waitingRequests = 0;

            try
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();

                        // **Combined Query for Total Requests:**

                        string query = "SELECT COUNT(*) " +
                                        "FROM BloodRequestHistory brh " +
                                        "JOIN Lookup l on brh.status = l.lookup_id " +
                                        "JOIN Donors d on (brh.matched_donor_id = d.donor_id OR brh.matched_donor_id IS NULL) " + // Modified JOIN
                                        "WHERE d.donor_id = @donorID AND brh.blood_type = d.blood_type " +
                                        "AND l.category = 'Blood Request Status'";

                        SqlCommand command = new SqlCommand(query, connection);
                        command.Parameters.AddWithValue("@donorID", donorID);

                        totalRequests = (int)command.ExecuteScalar();

                        // **Separate Query for Waiting Requests:**

                        query = "SELECT COUNT(*) " +
                                "FROM BloodRequestHistory brh " +
                                "JOIN Lookup l on brh.status = l.lookup_id " +
                                "JOIN Donors d on brh.matched_donor_id IS NULL " + // Filter for no matched donor ID
                                "WHERE d.donor_id = @donorID AND brh.blood_type = d.blood_type " +
                                "AND l.category = 'Blood Request Status' AND l.value = 'Waiting'";

                        command = new SqlCommand(query, connection);
                        command.Parameters.AddWithValue("@donorID", donorID);

                        waitingRequests = (int)command.ExecuteScalar();

                        // Update UI or perform further processing based on total and waiting requests
                    }
                }
                else
                {
                    // Handle the case where the connection string is not available
                    MessageBox.Show("Connection string could not be retrieved.");
                }
            }
            catch (SqlException ex)
            {
                // Handle SQL specific exceptions (e.g., connection errors)
                MessageBox.Show("An error occurred while retrieving data: " + ex.Message);
            }
            catch (Exception ex)  // Catch a broader exception for unexpected errors
            {
                // Handle general exceptions (e.g., casting errors)
                MessageBox.Show("An unexpected error occurred: " + ex.Message);
            }

            // You can return both totalRequests and waitingRequests
            return totalRequests;  // Or return an object containing both values
        }


        private int GetApprovedRequests()
        {
            int approvedRequests = 0;

            try
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();

                        string query = "SELECT COUNT(*) " +
                                        "FROM BloodRequestHistory brh " +
                                        "JOIN Lookup l on brh.status = l.lookup_id " +
                                        "JOIN Donors d on brh.matched_donor_id = d.donor_id " + // Assuming 'donor_id' is the correct name
                                        "WHERE d.donor_id = @donorID AND brh.blood_type = d.blood_type " +
                                        "AND l.category = 'Blood Request Status' AND l.value = 'Matched'";

                        SqlCommand command = new SqlCommand(query, connection);
                        command.Parameters.AddWithValue("@donorID", donorID);

                        approvedRequests = (int)command.ExecuteScalar();
                    }
                }
                else
                {
                    // Handle the case where the connection string is not available
                    MessageBox.Show("Connection string could not be retrieved.");
                }
            }
            catch (SqlException ex)
            {
                // Handle SQL specific exceptions (e.g., connection errors)
                MessageBox.Show("An error occurred while retrieving data: " + ex.Message);
            }
            catch (Exception ex)  // Catch a broader exception for unexpected errors
            {
                // Handle general exceptions (e.g., casting errors)
                MessageBox.Show("An unexpected error occurred: " + ex.Message);
            }

            return approvedRequests;
        }
    }
}
