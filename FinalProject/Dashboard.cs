using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Forms
{
    public partial class Dashboard : Form
    {
        private string _connectionString;

        public Dashboard()
        {
            InitializeComponent();
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;

            // Call a method to populate the Apositive TextBox
            PopulateApositiveCount();
            PopulateAnegativeCount();
            PopulateBpositiveCount();
            PopulateBnegativeCount();
            PopulateABpositiveCount();
            PopulateABnegativeCount();
            PopulateOpositiveCount();
            PopulateOnegativeCount();
            PopulateDonorRequestCounts();
        }

        private void PopulateApositiveCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 3);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    Apositive.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void PopulateAnegativeCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 4);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    Anegative.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void PopulateBpositiveCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 5);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    Bpositive.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void PopulateBnegativeCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 6);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    Bnegative.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }
        private void PopulateABpositiveCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 7);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    ABpositive.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }
        private void PopulateABnegativeCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 8);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    ABnegative.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }
        private void PopulateOpositiveCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 9);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    Opositive.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }
        private void PopulateOnegativeCount()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query to count donors with blood type A+
                    string query = "SELECT COUNT(*) FROM Donors WHERE blood_type  = @bloodtypeId";
                    SqlCommand command = new SqlCommand(query, connection);
                    // Assuming bloodtype_id for A+ is 1, replace it with the correct value if needed
                    command.Parameters.AddWithValue("@bloodtypeId", 10);

                    int aPositiveCount = (int)command.ExecuteScalar();
                    Onegative.Text = aPositiveCount.ToString();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }
        private void PopulateDonorRequestCounts()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Total donor requests count
                    string totalQuery = "SELECT COUNT(*) FROM Donors";
                    SqlCommand totalCommand = new SqlCommand(totalQuery, connection);
                    int totalDonorRequests = (int)totalCommand.ExecuteScalar();
                    totalTextBox.Text = totalDonorRequests.ToString();

                    // Pending donor requests count
                    string pendingQuery = "SELECT COUNT(*) FROM Donors WHERE donor_status = (SELECT lookup_id FROM Lookup WHERE value = 'Pending' AND category = 'Donor Request Status')";
                    SqlCommand pendingCommand = new SqlCommand(pendingQuery, connection);
                    int pendingDonorRequests = (int)pendingCommand.ExecuteScalar();
                    pendingTextBox.Text = pendingDonorRequests.ToString();

                    // Approved donor requests count
                    string approvedQuery = "SELECT COUNT(*) FROM Donors WHERE donor_status = (SELECT lookup_id FROM Lookup WHERE value = 'Approved' AND category = 'Donor Request Status')";
                    SqlCommand approvedCommand = new SqlCommand(approvedQuery, connection);
                    int approvedDonorRequests = (int)approvedCommand.ExecuteScalar();
                    approvedTextBox.Text = approvedDonorRequests.ToString();
                    // Approved donor requests count
                    string rejectQuery = "SELECT COUNT(*) FROM Donors WHERE donor_status = (SELECT lookup_id FROM Lookup WHERE value = 'Rejected' AND category = 'Donor Request Status')";
                    SqlCommand rejectCommand = new SqlCommand(rejectQuery, connection);
                    int rejectDonorRequests = (int)rejectCommand.ExecuteScalar();
                    RejectedTextBox.Text = rejectDonorRequests.ToString();
                   
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void guna2TextBox6_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
