using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.BL;

namespace WindowsFormsApp1.DL
{
    public class UserDAL
    {
        private readonly string _connectionString;

        public UserDAL()
        {
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
        }

        public int CreateUser(string username, string password, string userType)
        {
            // Validate user input
            if (!Validation.ValidateUsername(username))
            {
                throw new ArgumentException("Invalid username.");
            }

            if (!Validation.ValidatePassword(password))
            {
                throw new ArgumentException("Invalid password.");
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("CreateUser", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@username", username);
                        command.Parameters.AddWithValue("@hashedPassword", password);
                        command.Parameters.AddWithValue("@userType", userType);

                        object result = command.ExecuteScalar();

                        // Check if result is DBNull or null
                        if (result == DBNull.Value || result == null)
                        {
                            throw new Exception("User ID not returned from database.");
                        }

                        // Try parsing the result to int
                        if (!int.TryParse(result.ToString(), out int userId))
                        {
                            throw new Exception("Invalid user ID returned from database.");
                        }

                        return userId;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error registering user: {ex.Message}");
            }
        }


        public int SaveLocation(string streetAddress, string city, string province, string postalCode, string country)
        {
            // Validate address fields
            if (!Validation.AddressValidations(streetAddress, city, province, postalCode, country))
            {
                throw new ArgumentException("Invalid address information.");
            }

            string query = "SaveLocation";  // Call the stored procedure

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@StreetAddress", streetAddress);
                        command.Parameters.AddWithValue("@City", city);
                        command.Parameters.AddWithValue("@Province", province);
                        command.Parameters.AddWithValue("@PostalCode", postalCode);
                        command.Parameters.AddWithValue("@Country", country);

                        // ExecuteScalar to get the ID of the newly inserted location
                        int locationId = Convert.ToInt32(command.ExecuteScalar());

                        return locationId;
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving location information: {ex.Message}");
                }
            }
        }

        public void SavePersonInfo(int userId, string name, int gender, string mobileNumber,
                              string emailAddress, string dateOfBirth)
        {
            // Validate inputs
            if (!Validation.FirstNameValidations(name))
            {
                throw new ArgumentException("Invalid first name.");
            }

            if (!Validation.ContactValidations(mobileNumber))
            {
                throw new ArgumentException("Invalid mobile number.");
            }

            if (!Validation.EmailValidations(emailAddress))
            {
                throw new ArgumentException("Invalid email address.");
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("SavePersonInfo", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        // Add parameters
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@Name", name);
                        command.Parameters.AddWithValue("@Gender", gender);
                        command.Parameters.AddWithValue("@MobileNumber", mobileNumber);
                        command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                        command.Parameters.AddWithValue("@DateOfBirth", DateTime.Parse(dateOfBirth));

                        // Execute the stored procedure
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error saving person information: {ex.Message}");
            }
        }



        public void SaveDonorInfo(int userId, string lastDonationDate, int locationId, int currentLocationId, string willingness, int donorStatus, decimal weight, decimal height, int bloodTypeId)
        {
            // Validate inputs
            if (!Validation.ValidateLastDonationDate(lastDonationDate))
            {
                throw new ArgumentException("Invalid last donation date.");
            }

            // You can add additional validations for other parameters if needed

            string query = "SaveDonorInfo";  // Call the stored procedure

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@LastDonationDate", lastDonationDate);
                        command.Parameters.AddWithValue("@LocationId", locationId);
                        command.Parameters.AddWithValue("@CurrentLocationId", currentLocationId);
                        command.Parameters.AddWithValue("@Willingness", willingness);
                        command.Parameters.AddWithValue("@DonorStatus", donorStatus);
                        command.Parameters.AddWithValue("@Weight", weight);
                        command.Parameters.AddWithValue("@Height", height);
                        command.Parameters.AddWithValue("@BloodTypeId", bloodTypeId);

                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving donor information: {ex.Message}");
                }
            }
        }



        public void SaveMedicalHistory(int userId, string bloodTypeId, string infections, string allergies,
                                 string medications, string currentTreatment, string chronicDiseases, string surgeries)
        {
            // Validate inputs
            if (!Validation.ValidateInfections(infections))
            {
                throw new ArgumentException("Invalid infections.");
            }

            if (!Validation.ValidateAllergies(allergies))
            {
                throw new ArgumentException("Invalid allergies.");
            }

            if (!Validation.ValidateMedications(medications))
            {
                throw new ArgumentException("Invalid medications.");
            }

            if (!Validation.ValidateCurrentTreatment(currentTreatment))
            {
                throw new ArgumentException("Invalid current treatment.");
            }

            if (!Validation.ValidateChronicDiseases(chronicDiseases))
            {
                throw new ArgumentException("Invalid chronic diseases.");
            }

            if (!Validation.ValidateSurgeries(surgeries))
            {
                throw new ArgumentException("Invalid surgeries.");
            }

            // Validate blood type ID
            if (!Validation.BloodTypeValidations(bloodTypeId))
            {
                throw new ArgumentException("Invalid blood type ID.");
            }
            // You can add validations for other parameters as needed

            string query = "SaveMedicalHistory";  // Call the stored procedure

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserId", userId);
                      //  command.Parameters.AddWithValue("@BloodTypeId", bloodTypeId);
                        command.Parameters.AddWithValue("@Infections", infections);
                        command.Parameters.AddWithValue("@Allergies", allergies);
                        command.Parameters.AddWithValue("@Medications", medications);
                        command.Parameters.AddWithValue("@CurrentTreatment", currentTreatment);
                        command.Parameters.AddWithValue("@ChronicDiseases", chronicDiseases);
                        command.Parameters.AddWithValue("@Surgeries", surgeries);

                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving medical history: {ex.Message}");
                }
            }
        }

        public int SaveRecipientInfo(int userId, string streetAddress, string city, string province, string postalCode, string country, int bloodTypeId)
        {
            if (!Validation.AddressValidations(streetAddress, city, province, postalCode, country))
            {
                throw new ArgumentException("Invalid address information.");
            }

            // Save the location and retrieve its ID
            int locationId = SaveLocation(streetAddress, city, province, postalCode, country);

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("SaveRecipientInfo", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        // Add parameters
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@BloodTypeId", bloodTypeId);
                        command.Parameters.AddWithValue("@LocationId", locationId);

                        command.ExecuteNonQuery();
                    }
                }

                return locationId;
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error saving recipient information: {ex.Message}");
            }
        }


        public int SavePatientInfo(string name, int gender, string mobileNumber, string emailAddress, string dateOfBirth, int bloodTypeId, string hospitalName, string treatment, string attendingPhysician, int urgencyLevel, string streetAddress, string city, string province, string postalCode, string country)
{
    try
    {
        // Retrieve location ID
        int locationId = SaveLocation(streetAddress, city, province, postalCode, country);
                int patientId;
                // Insert patient information using stored procedure
                using (SqlConnection connection = new SqlConnection(_connectionString))
        {
                   
            connection.Open();
            using (SqlCommand command = new SqlCommand("SavePatient", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Name", name);
                command.Parameters.AddWithValue("@Gender", gender);
                command.Parameters.AddWithValue("@MobileNumber", mobileNumber);
                command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                command.Parameters.AddWithValue("@DateOfBirth", dateOfBirth);
                command.Parameters.AddWithValue("@BloodType", bloodTypeId);
                command.Parameters.AddWithValue("@AttendingPhysician", attendingPhysician);
                command.Parameters.AddWithValue("@HospitalName", hospitalName);
                command.Parameters.AddWithValue("@Treatment", treatment);
                command.Parameters.AddWithValue("@UrgencyLevelId", urgencyLevel);
                command.Parameters.AddWithValue("@LocationId", locationId);

                patientId = Convert.ToInt32(command.ExecuteScalar());
            }
        }

        return patientId;
    }
    catch (SqlException ex)
    {
        throw new Exception($"Error saving patient information: {ex.Message}");
    }
}


       
        public int ValidateUser(string username, string password)
        {
            // Validate inputs
            if (!Validation.ValidateUsername(username))
            {
                throw new ArgumentException("Invalid username.");
            }

            if (!Validation.ValidatePassword(password))
            {
                throw new ArgumentException("Invalid password.");
            }

            string query = "SELECT user_id FROM Users WHERE username = @username AND password = @hashedPassword";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@username", username);
                        command.Parameters.AddWithValue("@hashedPassword", password);

                        object result = command.ExecuteScalar();
                        if (result != null && result != DBNull.Value)
                        {
                            return Convert.ToInt32(result);
                        }
                        else
                        {
                            return 0; // or throw an exception
                        }
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error validating user: {ex.Message}");
                }
            }
        }


        public string GetUserType(int userId)
        {
            // Validate input
            if (userId <= 0)
            {
                throw new ArgumentException("Invalid user ID.");
            }

            string query = "SELECT userType FROM Users WHERE user_id = @userId";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);

                        string userType = (string)command.ExecuteScalar();
                        return userType;
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error getting user type: {ex.Message}");
                }
            }
        }
        public void AddBloodRequestHistory(int recipientId, int patientId, DateTime dateRequested, int statusId, int bloodTypeId)
        {
            try
            {
                string query = "AddBloodRequestHistory";  // Call the stored procedure

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RecipientId", recipientId);
                        command.Parameters.AddWithValue("@PatientId", patientId);
                        command.Parameters.AddWithValue("@DateRequested", dateRequested);
                        command.Parameters.AddWithValue("@StatusId", statusId);
                        command.Parameters.AddWithValue("@BloodTypeId", bloodTypeId);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error adding blood request history: {ex.Message}");
            }
        }

    
        public int GetRecipientIdByUserId(int userId)
        {
            try
            {
                string query = "SELECT recipient_id FROM Recipients WHERE recipient_id = @userId";

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);
                        object result = command.ExecuteScalar();
                        if (result != null && result != DBNull.Value)
                        {
                            return Convert.ToInt32(result);
                        }
                        else
                        {
                            // If the user is not found in the Recipients table, return -1 or throw an exception
                            return -1;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error getting recipient ID: {ex.Message}");
            }
        }

        public int GetDonorIdByUserId(int userId)
        {
            try
            {
                string query = "SELECT donor_id FROM Donors WHERE donor_id = @userId";

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);
                        object result = command.ExecuteScalar();
                        if (result != null && result != DBNull.Value)
                        {
                            return Convert.ToInt32(result);
                        }
                        else
                        {
                            // If the user is not found in the Recipients table, return -1 or throw an exception
                            return -1;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error getting recipient ID: {ex.Message}");
            }
        }

        public int GetBloodRequestStatusId(string status)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string query = "SELECT lookup_id FROM Lookup WHERE category = 'Blood Request Status' AND value = @Status";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Status", status);
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            return Convert.ToInt32(result);
                        }
                        else
                        {
                            // Handle if the status is not found
                            return -1; // Or throw an exception
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error getting blood request status ID: {ex.Message}");
            }
        }

        // Method to retrieve lookup value from the database
        public string GetLookupValue(string category, int id)
        {
            string value = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    // Query the Lookup table to get the value
                    string query = "SELECT value FROM Lookup WHERE category = @category AND lookup_id = @id";
                    SqlCommand command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@category", category);
                    command.Parameters.AddWithValue("@id", id);

                    object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        value = result.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions appropriately (e.g., logging)
            }
            return value;
        }
    }


}


