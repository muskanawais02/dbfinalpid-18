﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Forms
{
    public partial class ApproveDonor : Form
    {
        private string _connectionString;

        public ApproveDonor()
        {
            InitializeComponent();
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;

            // Load donor data and set the data source
            LoadDonorData();

            // Add Approve and Reject buttons to the DataGridView
            DonorGrid.Columns.Add(new DataGridViewButtonColumn
            {
                Name = "ApproveButton",
                Text = "Approve",
                UseColumnTextForButtonValue = true
            });
            DonorGrid.Columns.Add(new DataGridViewButtonColumn
            {
                Name = "RejectButton",
                Text = "Reject",
                UseColumnTextForButtonValue = true
            });

            // Handle button clicks in the DataGridView
            DonorGrid.CellClick += DonorGrid_CellClick;
        }

        private void ApproveDonor_Load(object sender, EventArgs e)
        {
        }

        private void LoadDonorData()
        {
            DataTable table = new DataTable();

            // Define the schema of the DataTable
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Age", typeof(int));
            table.Columns.Add("Height", typeof(decimal));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Willingness", typeof(string));
          
            table.Columns.Add("City", typeof(string));
            table.Columns.Add("Country", typeof(string));
            table.Columns.Add("Infections", typeof(string));
            table.Columns.Add("Allergies", typeof(string));
            table.Columns.Add("Medications", typeof(string));
       
            table.Columns.Add("Surgeries", typeof(string));

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                string query = "SELECT * FROM DonorDataView"; // Fetch data from the view
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        table.Load(reader);
                    }
                }
            }
            DonorGrid.DataSource = table;
        }

        private void DonorGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && (DonorGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn))
            {
                // Get the selected donor's ID from the database based on the row index
                int donorId = GetDonorIdFromDatabase(e.RowIndex);

                // Check if the clicked button is Approve or Reject
                string buttonClicked = DonorGrid.Columns[e.ColumnIndex].Name;

                if (buttonClicked == "ApproveButton")
                {
                    UpdateDonorStatus(donorId, "Approved");
                }
                else if (buttonClicked == "RejectButton")
                {
                    UpdateDonorStatus(donorId, "Rejected");
                }
            }
        }
        private int GetDonorIdFromDatabase(int rowIndex)
        {
            int donorId = -1; // Default value

            // Establish a connection to your database
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                // Define your SQL query to fetch the donor ID
                string query = "SELECT donor_id FROM Donors WHERE donor_status = (SELECT lookup_id FROM Lookup WHERE value = 'Pending' AND category = 'Donor Request Status')";

                // Create a SqlCommand object with the query and the connection
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Execute the query and retrieve the donor ID
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        // Move to the row corresponding to the given rowIndex
                        for (int i = 0; i <= rowIndex && reader.Read(); i++)
                        {
                            if (i == rowIndex)
                            {
                                donorId = reader.GetInt32(0); // Assuming the donor_id is in the first column
                                break;
                            }
                        }
                    }
                }
            }

            return donorId;
        }

        private void UpdateDonorStatus(int donorId, string newStatus)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string updateQuery = @"
UPDATE Donors
SET donor_status = (SELECT lookup_id FROM Lookup WHERE value = @newStatus AND category = 'Donor Request Status')
WHERE donor_id = @donorId";

                using (SqlCommand command = new SqlCommand(updateQuery, connection))
                {
                    command.Parameters.AddWithValue("@donorId", donorId);
                    command.Parameters.AddWithValue("@newStatus", newStatus);
                    command.ExecuteNonQuery();
                }
            }

            // Reload donor data to reflect the update
            LoadDonorData();
        }
    }
}
