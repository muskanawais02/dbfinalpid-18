﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1.Forms
{
    public partial class AddFeedBack : Form
    {
        private readonly FeedBackDL _feedbackBL;


        public AddFeedBack()
        {
            InitializeComponent();
            _feedbackBL = new FeedBackDL();

        }

       

        private void Search_Click_1(object sender, EventArgs e)
        {

            string feedbackText = FeedBackRichTextBox.Text.Trim();
            if (string.IsNullOrEmpty(feedbackText))
            {
                MessageBox.Show("Please enter feedback before submitting.");
                return;
            }

            try
            {
                int recipientId = SessionManager.RecipientId;
                _feedbackBL.AddFeedback(recipientId, feedbackText);
                MessageBox.Show("Feedback submitted successfully.");
                FeedBackRichTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
