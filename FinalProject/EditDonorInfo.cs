using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1.Forms
{
    public partial class EditDonorInfo : Form
    {
        private readonly UserDAL _userDAL;
        private readonly UserBL _userBL;
        private readonly string connectionString;



        public EditDonorInfo()
        {
            InitializeComponent();
            _userDAL = new UserDAL();
            _userBL = new UserBL();
            SqlConnection connection = Configuration.getInstance().getConnection();
            connectionString = connection.ConnectionString;
            int donorId = SessionManager.DonorId;
            LoadDonorInfo(donorId);
        }
        private void LoadDonorInfo(int donorId)
        {
            try
            {
                // Fetch the donor information from the database
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    // Fetch the person information
                    string personQuery = "SELECT p.name, p.gender, p.mobile_number, p.email_address, p.date_of_birth " +
                                         "FROM Person p " +
                                         "WHERE p.person_id = (SELECT donor_id FROM Donors WHERE donor_id = @DonorId)";
                    var personCommand = new SqlCommand(personQuery, connection);
                    personCommand.Parameters.AddWithValue("@DonorId", donorId);
                    using (var personReader = personCommand.ExecuteReader())
                    {
                        if (personReader.Read())
                        {
                            nameTextBox.Text = personReader.GetString(0);
                            genderComboBox.SelectedItem = _userDAL.GetLookupValue("Gender", personReader.GetInt32(1));
                            mobileNumberTextBox.Text = personReader.GetString(2);
                            emailAddressTextBox.Text = personReader.GetString(3);
                            dateOfBirthPicker.Value = personReader.GetDateTime(4);
                        }
                    }

                    // Fetch the location information
                    string locationQuery = "SELECT l.street_address, l.city, l.province, l.postal_code, l.country " +
                                           "FROM Locations l " +
                                           "WHERE l.location_id = (SELECT location_id FROM Donors WHERE donor_id = @DonorId)";
                    var locationCommand = new SqlCommand(locationQuery, connection);
                    locationCommand.Parameters.AddWithValue("@DonorId", donorId);
                    using (var locationReader = locationCommand.ExecuteReader())
                    {
                        if (locationReader.Read())
                        {
                            streetAddressTextBox.Text = locationReader.GetString(0);
                            cityComboBox.SelectedItem = locationReader.GetString(1);
                            provinceCombox.SelectedItem = locationReader.GetString(2);
                            postalCodeTextBox.Text = locationReader.GetString(3);
                            countryComboBox.SelectedItem = locationReader.GetString(4);
                        }
                    }

                    // Fetch the medical history information
                    string medicalHistoryQuery = "SELECT i.infections, i.allergies, i.medications, i.currentTreatment, i.chronicdiseases, i.surgeries " +
                                                 "FROM MedicalHistory i " +
                                                 "WHERE i.medical_history_id = @DonorId";
                    var medicalHistoryCommand = new SqlCommand(medicalHistoryQuery, connection);
                    medicalHistoryCommand.Parameters.AddWithValue("@DonorId", donorId);
                    using (var medicalHistoryReader = medicalHistoryCommand.ExecuteReader())
                    {
                        if (medicalHistoryReader.Read())
                        {
                            infectionsTextBox.Text = medicalHistoryReader.GetString(0);
                            allergiesTextBox.Text = medicalHistoryReader.GetString(1);
                            medicationsTextBox.Text = medicalHistoryReader.GetString(2);
                            currentTreatmentTextBox.Text = medicalHistoryReader.GetString(3);
                            chronicDiseasesTextBox.Text = medicalHistoryReader.GetString(4);
                            surgeriesTextBox.Text = medicalHistoryReader.GetString(5);
                        }
                    }

                    // Fetch the donor-specific information
                    string donorQuery = "SELECT d.last_donation_date, d.willingness, d.weight, d.height, d.blood_type " +
                                        "FROM Donors d " +
                                        "WHERE d.donor_id = @DonorId";
                    var donorCommand = new SqlCommand(donorQuery, connection);
                    donorCommand.Parameters.AddWithValue("@DonorId", donorId);
                    using (var donorReader = donorCommand.ExecuteReader())
                    {
                        if (donorReader.Read())
                        {
                            lastDonationDatePicker.Value = donorReader.GetDateTime(0);
                            willingnessComboBox.SelectedItem = donorReader.GetString(1);
                            weightTextBox.Text = donorReader.GetDecimal(2).ToString();
                            heightTextBox.Text = donorReader.GetDecimal(3).ToString();
                            bloodTypeComboBox.SelectedItem = _userDAL.GetLookupValue("Blood Type", Convert.ToInt32(donorReader["blood_type"]));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle any exceptions
                MessageBox.Show($"An error occurred: {ex.Message}", "Error in loading the donor personal information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void gunaButton1_Click(object sender, EventArgs e)
        {
            try
            {
                // Get the donor ID from the session
                int donorId = SessionManager.DonorId;

                // Update the person information
                string personQuery = "UPDATE Person " +
                                     "SET name = @name, gender = @gender, mobile_number = @mobileNumber, email_address = @emailAddress, date_of_birth = @dateOfBirth " +
                                     "WHERE person_id = @donorId";

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    var personCommand = new SqlCommand(personQuery, connection);
                    // int genderId = _userBL.GetGenderId(genderComboBox.SelectedItem.ToString());
                    personCommand.Parameters.AddWithValue("@name", nameTextBox.Text);
                    personCommand.Parameters.AddWithValue("@gender", _userBL.GetGenderId(genderComboBox.SelectedItem.ToString()));
                    personCommand.Parameters.AddWithValue("@mobileNumber", mobileNumberTextBox.Text);
                    personCommand.Parameters.AddWithValue("@emailAddress", emailAddressTextBox.Text);
                    personCommand.Parameters.AddWithValue("@dateOfBirth", dateOfBirthPicker.Value);
                    personCommand.Parameters.AddWithValue("@donorId", donorId);
                    personCommand.ExecuteNonQuery();
                }

                // Update the location information
                string locationQuery = "UPDATE Locations " +
                                       "SET street_address = @streetAddress, city = @city, province = @province, postal_code = @postalCode, country = @country " +
                                       "WHERE location_id = (SELECT location_id FROM Donors WHERE donor_id = @donorId)";

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    var locationCommand = new SqlCommand(locationQuery, connection);
                    locationCommand.Parameters.AddWithValue("@streetAddress", streetAddressTextBox.Text);
                    locationCommand.Parameters.AddWithValue("@city", cityComboBox.SelectedItem.ToString());
                    locationCommand.Parameters.AddWithValue("@province", provinceCombox.SelectedItem.ToString());
                    locationCommand.Parameters.AddWithValue("@postalCode", postalCodeTextBox.Text);
                    locationCommand.Parameters.AddWithValue("@country", countryComboBox.SelectedItem.ToString());
                    locationCommand.Parameters.AddWithValue("@donorId", donorId);
                    locationCommand.ExecuteNonQuery();
                }

                // Update the medical history information
                string medicalHistoryQuery = "UPDATE MedicalHistory " +
                                             "SET infections = @infections, allergies = @allergies, medications = @medications, currentTreatment = @currentTreatment, chronicdiseases = @chronicDiseases, surgeries = @surgeries " +
                                             "WHERE medical_history_id = @donorId";

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    var medicalHistoryCommand = new SqlCommand(medicalHistoryQuery, connection);
                    medicalHistoryCommand.Parameters.AddWithValue("@infections", infectionsTextBox.Text);
                    medicalHistoryCommand.Parameters.AddWithValue("@allergies", allergiesTextBox.Text);
                    medicalHistoryCommand.Parameters.AddWithValue("@medications", medicationsTextBox.Text);
                    medicalHistoryCommand.Parameters.AddWithValue("@currentTreatment", currentTreatmentTextBox.Text);
                    medicalHistoryCommand.Parameters.AddWithValue("@chronicDiseases", chronicDiseasesTextBox.Text);
                    medicalHistoryCommand.Parameters.AddWithValue("@surgeries", surgeriesTextBox.Text);
                    medicalHistoryCommand.Parameters.AddWithValue("@donorId", donorId);
                    medicalHistoryCommand.ExecuteNonQuery();
                }

                // Update the donor-specific information
                string donorQuery = "UPDATE Donors " +
                                    "SET last_donation_date = @lastDonationDate, willingness = @willingness, weight = @weight, height = @height, blood_type = @bloodType " +
                                    "WHERE donor_id = @donorId";

                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    var donorCommand = new SqlCommand(donorQuery, connection);
                    donorCommand.Parameters.AddWithValue("@lastDonationDate", lastDonationDatePicker.Value);
                    donorCommand.Parameters.AddWithValue("@willingness", willingnessComboBox.SelectedItem.ToString());
                    donorCommand.Parameters.AddWithValue("@weight", decimal.Parse(weightTextBox.Text));
                    donorCommand.Parameters.AddWithValue("@height", decimal.Parse(heightTextBox.Text));
                    donorCommand.Parameters.AddWithValue("@bloodType", _userBL.GetBloodTypeId(bloodTypeComboBox.SelectedItem.ToString()));
                    donorCommand.Parameters.AddWithValue("@donorId", donorId);
                    donorCommand.ExecuteNonQuery();
                }

                // Show a success message
                MessageBox.Show("Donor information updated successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                // Handle any exceptions
                MessageBox.Show($"An error occurred: {ex.Message}", "Error in updating the donor personal information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
