﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AdminMenu : Form
    {
        private Button currentButton;
        private Random random;
        private int tempIndex;
        private Form activeForm;

        private Color originalBackColor; // Store original button back color
        private Color originalForeColor; // Store original button fore color

        public AdminMenu()
        {
            InitializeComponent();

            // Call the function to set hover effects for all buttons
            SetHoverEffectsForButtons(this);

            // Store original colors (assuming all buttons have the same defaults)
            originalBackColor = DashBoard.BackColor;
            originalForeColor = DashBoard.ForeColor;
        }

        public void OpenChildForm(Form childForm, object sender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }

            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktopPane.Controls.Add(childForm);
            this.panelDesktopPane.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            label1.Text = childForm.Text;

            // ... rest of your OpenChildForm code
        }
        private void SetHoverEffectsForButtons(Control container)
        {
            // Loop through all controls in the container
            foreach (Control control in container.Controls)
            {
                // If the control is a container (like Panel or GroupBox), recursively call SetHoverEffectsForButtons
                if (control.HasChildren)
                {
                    SetHoverEffectsForButtons(control);
                }

                // Check if the control is a button
                if (control is Button button)
                {
                    // Add event handlers for the button
                    button.MouseEnter += Button_MouseEnter;
                    button.MouseLeave += Button_MouseLeave;
                }
            }
        }

        private void Button_MouseEnter(object sender, EventArgs e)
        {
            Button button = (Button)sender; // Cast sender to Button

            // Change back and fore color on hover (adjust colors as desired)
            button.BackColor = Color.DimGray;
            button.ForeColor = Color.White;
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender; // Cast sender to Button

            // Reset back and fore color to original on leave
            button.BackColor = originalBackColor;
            button.ForeColor = originalForeColor;
        }

        private void DashBoard_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.Dashboard(), sender);
        }

        private void panelDesktopPane_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AdminMenu_Load(object sender, EventArgs e)
        {
          
        }

        private void Donors_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.ViewDonor(), sender);
        }

        private void Recipient_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.ViewRecipients(), sender);
        }

        private void Donations_Click(object sender, EventArgs e)
        {
             OpenChildForm(new Forms.Donations(), sender);
        }

        private void BloodRequests_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.ViewBloodRequests(), sender);
        }

        private void DonorRequest_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.DonorBloodRequest(), sender);
        }

        private void Locations_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.TrackLocations(), sender);
        }

        private void Reports_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.Report(), sender);
        }

        private void ViewProfile_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.AdminProfile(), sender);
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.ViewFeedBacks(), sender);
        }
    }
}
