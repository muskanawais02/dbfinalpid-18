﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BL;

namespace WindowsFormsApp1.Forms
{
    public partial class EditDonorInfo : Form
    {
        private readonly UserBL _userBL;
        private readonly int _userId;

        public EditDonorInfo(int userId)
        {
            InitializeComponent();
            _userBL = new UserBL();
            _userId = userId;
            PrefillDonorInfo();
        }

        public EditDonorInfo()
        {
        }

        private void PrefillDonorInfo()
        {
            try
            {
                // Retrieve donor's information from the database
                DataTable donorInfo = _userBL.SavePersonInfo(_userId);

                if (donorInfo.Rows.Count > 0)
                {
                    DataRow donorRow = donorInfo.Rows[0];

                    // Personal Information
                    nameTextBox.Text = donorRow["name"].ToString();
                    genderComboBox.SelectedItem = donorRow["gender"].ToString(); // Assuming gender is stored as a string
                    mobileNumberTextBox.Text = donorRow["mobile_number"].ToString();
                    emailAddressTextBox.Text = donorRow["email_address"].ToString();
                    dateOfBirthPicker.Value = Convert.ToDateTime(donorRow["date_of_birth"]);

                    // Location Information (assuming separate table)
                    DataTable locationInfo = _userBL.GetLocation(_userId); // Call a separate method for location

                    if (locationInfo.Rows.Count > 0)
                    {
                        DataRow locationRow = locationInfo.Rows[0];
                        streetAddressTextBox.Text = locationRow["street_address"].ToString();
                        cityComboBox.SelectedItem = locationRow["city"].ToString();
                        provinceCombox.SelectedItem = locationRow["province"].ToString();
                        postalCodeTextBox.Text = locationRow["postal_code"].ToString();
                        countryComboBox.SelectedItem = locationRow["country"].ToString();
                    }

                    // Medical History Information (assuming separate table)
                    DataTable medicalHistory = _userBL.GetMedicalHistory(_userId); // Call a separate method for medical history

                    if (medicalHistory.Rows.Count > 0)
                    {
                        DataRow medicalRow = medicalHistory.Rows[0];
                        bloodTypeComboBox.SelectedItem = medicalRow["blood_type_id"].ToString(); // Assuming blood type ID is stored
                        infectionsTextBox.Text = medicalRow["infections"].ToString();
                        allergiesTextBox.Text = medicalRow["allergies"].ToString();
                        medicationsTextBox.Text = medicalRow["medications"].ToString();
                        currentTreatmentTextBox.Text = medicalRow["current_treatment"].ToString();
                        chronicDiseasesTextBox.Text = medicalRow["chronic_diseases"].ToString();
                        surgeriesTextBox.Text = medicalRow["surgeries"].ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Donor information not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while prefilling donor information: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gunaButton1_Click(object sender, EventArgs e)
        {

        }

      
    }
}
