﻿using iTextSharp.xmp.impl;
using System;
using System.Net.Mail;
using System.Windows.Forms;
using System.Xml.Linq;
using WindowsFormsApp1;
using WindowsFormsApp1.BL;

namespace BloodDonationSystem
{
    public partial class RecipientPersonalInfo : Form
    {
        private readonly UserBL _userBL;
        private readonly int _userId;

        public RecipientPersonalInfo(int userId)
        {
            InitializeComponent();
            _userBL = new UserBL();
            _userId = userId;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
           
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            string streetAddress = streetAddressTextBox.Text;
            string city = cityComboBox.SelectedItem.ToString();
            string province = provinceCombox.SelectedItem.ToString();
            string postalCode = postalCodeTextBox.Text;
            string country = countryComboBox.SelectedItem.ToString();

            string name = nameTextBox.Text;
            string gender = genderComboBox.SelectedItem.ToString();
            string mobileNumber = mobileNumberTextBox.Text;
            string emailAddress = emailAddressTextBox.Text;
            DateTime dateOfBirth = dateOfBirthPicker.Value;

            string urgencyLevel = urgencyLevelComboBox.SelectedItem.ToString();
            string bloodType = bloodTypeComboBox.SelectedItem.ToString();

            try
            {
                // Save recipient information
                _userBL.SaveRecipientInfo(_userId, streetAddress, city, province, postalCode, country, urgencyLevel, bloodType);

                // Save person information
                _userBL.SavePersonInfo(_userId, name, gender, mobileNumber, emailAddress, dateOfBirth);

                MessageBox.Show("Recipient information saved successfully!");

                // Close the form or perform other actions after successful save
                this.Close();

                // Open the sign-in form
                SignIn signInForm = new SignIn();
                signInForm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error saving recipient information: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
