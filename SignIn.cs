﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1
{
    public partial class SignIn : Form
    {
        private UserDAL _userDAL; // Instance of UserDAL class for database access

        public SignIn()
        {
            InitializeComponent();
            _userDAL = new UserDAL(); // Initialize UserDAL
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            if (Show.Text == "show")
            {
                Show.Text = "hide";
                txtpassword.PasswordChar = '\0';
            }
            else
            {
                Show.Text = "show";
                txtpassword.PasswordChar = '*';
            }
        }

        private void Signup_Click(object sender, EventArgs e)
        {
        }

        private void Login_Click(object sender, EventArgs e)
        {
            string username = txtusername.Text;
            string password = txtpassword.Text;

            try
            {
                int userId = _userDAL.ValidateUser(username, password); // Call method to validate user

                if (userId > 0) // User found
                {
                    string userType = _userDAL.GetUserType(userId); // Get user type

                    if (userType == "Donor")
                    {
                        // Open Donor Menu (replace with your Donor Menu form code)
                        DonorMenu donorMenu = new DonorMenu();
                        donorMenu.Show();
                    }
                    else if (userType == "Recipient")
                    {
                        // Open Recipient Menu (replace with your Recipient Menu form code)
                        RecipientMenu recipientMenu = new RecipientMenu();
                        recipientMenu.Show();
                    }
                    else
                    {
                        MessageBox.Show("Invalid user type found.");
                    }

                    this.Hide(); // Hide login form
                }
                else
                {
                    MessageBox.Show("Invalid username or password.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error logging in: " + ex.Message);
            }
        

        }
    }
}
