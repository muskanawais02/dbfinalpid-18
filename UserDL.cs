﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DL
{
    public class UserDAL
    {
        private readonly string _connectionString;

        public UserDAL()
        {
            SqlConnection connection = Configuration.getInstance().getConnection();
            _connectionString = connection.ConnectionString;
        }

        public int CreateUser(string username, string password, string userType)
        {

            // Validate user input
            if (!Validation.ValidateUsername(username))
            {
                throw new ArgumentException("Invalid username.");
            }

            if (!Validation.ValidatePassword(password))
            {
                throw new ArgumentException("Invalid password.");
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    string query = @"
                        INSERT INTO Users (username, password, userType)
                        OUTPUT INSERTED.user_id
                        VALUES (@username, @hashedPassword, @userType)";
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@username", username);
                        command.Parameters.AddWithValue("@hashedPassword", password);
                        command.Parameters.AddWithValue("@userType", userType);

                        int userId = (int)command.ExecuteScalar();

                        return userId;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error registering user: {ex.Message}");
            }
        }
        public int SaveLocation(string streetAddress, string city, string province, string postalCode, string country)
        {
            // Validate address fields
            if (!Validation.AddressValidations(streetAddress, city, province, postalCode, country))
            {
                throw new ArgumentException("Invalid address information.");
            }

            string query = @"
        INSERT INTO Locations (street_address, city, province, postal_code, country) 
        VALUES (@streetAddress, @city, @province, @postalCode, @country);
        SELECT CAST(SCOPE_IDENTITY() AS INT);";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@streetAddress", streetAddress);
                        command.Parameters.AddWithValue("@city", city);
                        command.Parameters.AddWithValue("@province", province);
                        command.Parameters.AddWithValue("@postalCode", postalCode);
                        command.Parameters.AddWithValue("@country", country);

                        // ExecuteScalar to get the ID of the newly inserted location
                        int locationId = Convert.ToInt32(command.ExecuteScalar());

                        return locationId;
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving location information: {ex.Message}");
                }
            }
        }

        public void SavePersonInfo(int userId, string name, int gender, string mobileNumber,
                                    string emailAddress, DateTime dateOfBirth)
        {
            // Validate inputs
            if (!Validation.FirstNameValidations(name))
            {
                throw new ArgumentException("Invalid first name.");
            }

            if (!Validation.ContactValidations(mobileNumber))
            {
                throw new ArgumentException("Invalid mobile number.");
            }

            if (!Validation.EmailValidations(emailAddress))
            {
                throw new ArgumentException("Invalid email address.");
            }

            if (!Validation.DoBValidations(dateOfBirth.ToString(), 2006, 2005)) // Specify minYear and maxYear
            {
                throw new ArgumentException("Invalid date of birth.");
            }

            string query = $"INSERT INTO Person (person_id, name, gender, mobile_number, email_address, date_of_birth) " +
                            $"VALUES (@userId, @name, @gender, @mobileNumber, @emailAddress, @dateOfBirth)";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@gender", gender); // Corrected line
                        command.Parameters.AddWithValue("@mobileNumber", mobileNumber);
                        command.Parameters.AddWithValue("@emailAddress", emailAddress);
                        command.Parameters.AddWithValue("@dateOfBirth", dateOfBirth);
                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving person information: {ex.Message}");
                }
            }
        }

        public void SaveDonorInfo(int userId, string lastDonationDate, int locationId, int currentLocationId, string willingness, int donorStatus, decimal weight, decimal height, int bloodTypeId)
        {
            // Validate inputs
            if (!Validation.ValidateLastDonationDate(lastDonationDate))
            {
                throw new ArgumentException("Invalid last donation date.");
            }

            // You can add additional validations for other parameters if needed

            string query = @"
        INSERT INTO Donors (donor_id, last_donation_date, location_id, current_location_id, willingness, donor_status, weight, height, blood_type) 
        VALUES (@userId, @lastDonationDate, @locationId, @currentLocationId, @willingness, @donorStatus, @weight, @height, @bloodTypeId)";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);
                        command.Parameters.AddWithValue("@lastDonationDate", lastDonationDate);
                        command.Parameters.AddWithValue("@locationId", locationId);
                        command.Parameters.AddWithValue("@currentLocationId", currentLocationId);
                        command.Parameters.AddWithValue("@willingness", willingness);
                        command.Parameters.AddWithValue("@donorStatus", donorStatus);
                        command.Parameters.AddWithValue("@weight", weight);
                        command.Parameters.AddWithValue("@height", height);
                        command.Parameters.AddWithValue("@bloodTypeId", bloodTypeId);

                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving donor information: {ex.Message}");
                }
            }
        }


        public void SaveMedicalHistory(int userId, string bloodTypeId, string infections, string allergies,
                                 string medications, string currentTreatment, string chronicDiseases, string surgeries)
        {
            // Validate inputs
            if (!Validation.ValidateInfections(infections))
            {
                throw new ArgumentException("Invalid infections.");
            }

            if (!Validation.ValidateAllergies(allergies))
            {
                throw new ArgumentException("Invalid allergies.");
            }

            if (!Validation.ValidateMedications(medications))
            {
                throw new ArgumentException("Invalid medications.");
            }

            if (!Validation.ValidateCurrentTreatment(currentTreatment))
            {
                throw new ArgumentException("Invalid current treatment.");
            }

            if (!Validation.ValidateChronicDiseases(chronicDiseases))
            {
                throw new ArgumentException("Invalid chronic diseases.");
            }

            if (!Validation.ValidateSurgeries(surgeries))
            {
                throw new ArgumentException("Invalid surgeries.");
            }

            // Validate blood type ID
            if (!Validation.BloodTypeValidations(bloodTypeId))
            {
                throw new ArgumentException("Invalid blood type ID.");
            }
            // You can add validations for other parameters as needed

            string query = $"INSERT INTO MedicalHistory (medical_history_id, infections, allergies, medications, currentTreatment, chronicdiseases, surgeries) " +
                           $"VALUES (@userId, @infections, @allergies, @medications, @currentTreatment, @chronicDiseases, @surgeries)";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);
                        command.Parameters.AddWithValue("@infections", infections);
                        command.Parameters.AddWithValue("@allergies", allergies);
                        command.Parameters.AddWithValue("@medications", medications);
                        command.Parameters.AddWithValue("@currentTreatment", currentTreatment);
                        command.Parameters.AddWithValue("@chronicDiseases", chronicDiseases);
                        command.Parameters.AddWithValue("@surgeries", surgeries);
                        command.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error saving medical history: {ex.Message}");
                }
            }
        }

        public int SaveRecipientInfo(int userId, string streetAddress, string city, string province, string postalCode, string country, int urgencyLevel, int bloodTypeId)
        {
            if (!Validation.AddressValidations(streetAddress, city, province, postalCode, country))
            {
                throw new ArgumentException("Invalid address information.");
            }

            // Validate urgency level
            if (urgencyLevel < 1 || urgencyLevel > 10)
            {
                throw new ArgumentException("Invalid urgency level. Urgency level must be between 1 and 10.");
            }

           


            // You can add validations for other parameters as needed

            // Save the location and retrieve its ID
            int locationId = SaveLocation(streetAddress, city, province, postalCode, country);

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    string query = @"
                INSERT INTO Recipients (recipient_id, patient_id, urgency_level, blood_type, location_id, current_location_id)
                VALUES (@userId, NULL, @urgencyLevel, @bloodTypeId, @locationId, @currentLocationId)";

                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);
                        command.Parameters.AddWithValue("@urgencyLevel", urgencyLevel);
                        command.Parameters.AddWithValue("@bloodTypeId", bloodTypeId);
                        command.Parameters.AddWithValue("@locationId", locationId);
                        command.Parameters.AddWithValue("@currentLocationId", locationId);

                        command.ExecuteNonQuery();
                    }
                }

                return locationId;
            }
            catch (SqlException ex)
            {
                throw new Exception($"Error saving recipient information: {ex.Message}");
            }
        }

        public int ValidateUser(string username, string password)
        {
            // Validate inputs
            if (!Validation.ValidateUsername(username))
            {
                throw new ArgumentException("Invalid username.");
            }

            if (!Validation.ValidatePassword(password))
            {
                throw new ArgumentException("Invalid password.");
            }

            string query = "SELECT user_id FROM Users WHERE username = @username AND password = @hashedPassword";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@username", username);
                        command.Parameters.AddWithValue("@hashedPassword", password);

                        int userId = (int)command.ExecuteScalar();
                        return userId;
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error validating user: {ex.Message}");
                }
            }
        }

        public string GetUserType(int userId)
        {
            // Validate input
            if (userId <= 0)
            {
                throw new ArgumentException("Invalid user ID.");
            }

            string query = "SELECT userType FROM Users WHERE user_id = @userId";

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@userId", userId);

                        string userType = (string)command.ExecuteScalar();
                        return userType;
                    }
                }
                catch (SqlException ex)
                {
                    throw new Exception($"Error getting user type: {ex.Message}");
                }
            }
        }


    }
    }
