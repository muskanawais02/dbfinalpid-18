﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using WindowsFormsApp1.Forms;
using WindowsFormsApp1.DL;

namespace WindowsFormsApp1.BL
{
    class PatientBL
    {
        public string FullName { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string BloodType { get; set; }
        public string StreetAddress { get; set; }
        public string Province { get; set; }
        public string UrgencyLevel { get; set; }
        public string MedicalCondition { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string Country { get; set; }
        public string HospitalName { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string MedicalConditionRequiringBloodDonation { get; set; }

        // Constructor with parameters matching the properties
        public PatientBL(string fullName, string gender, DateTime dateOfBirth, string bloodType,
            string streetAddress, string province, string urgencyLevel, string medicalCondition,
            string emailAddress, string mobileNumber, string country, string hospitalName,
            string city, string postalCode, string medicalConditionRequiringBloodDonation)
        {
            FullName = fullName;
            Gender = gender;
            DateOfBirth = dateOfBirth;
            BloodType = bloodType;
            StreetAddress = streetAddress;
            Province = province;
            UrgencyLevel = urgencyLevel;
            MedicalCondition = medicalCondition;
            EmailAddress = emailAddress;
            MobileNumber = mobileNumber;
            Country = country;
            HospitalName = hospitalName;
            City = city;
            PostalCode = postalCode;
            MedicalConditionRequiringBloodDonation = medicalConditionRequiringBloodDonation;
        }
    }

}

